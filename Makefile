OBJS_CPP = Button.o Component.o Menu.o Puzzle.o TaquinApplication.o Tile.o main.o


all : exec

INCLUDE=./SFML-2.5.1/include
LIB=./SFML-2.5.1/lib
PP=g++ 


exec : $(OBJS_CPP)
	$(PP) $^ -o $@ -L"$(LIB)" -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio




%.o: ./src/%.cpp
	$(PP) -c $< -I"$(INCLUDE)" -o $@







clean :
	rm -f $(OBJS_CPP)
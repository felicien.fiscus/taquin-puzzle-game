# Taquin Puzzle Game

A Taquin puzzle game using C++ SFML version 2.5.1.
You need SFML2.5.1 installed on your IDE to compile the code successfully.
How to use: The resulting executable can be given a string input that will be interpreted as the file path to the
image you want to use as a taquin puzzle. The string given is optional, if none is set, then the Images/default.png will be taken as the puzzle.

The user inputs are the mouse dragging for moving the tiles and 'S' to enable/disable the shuffle of tiles. You can also go back to the menu
when in game by pressing Esc key.

To play just download the content in the executable directory and put the directories Fonts, Textures, SoundEffects and Images in the same directory as where the executable is.

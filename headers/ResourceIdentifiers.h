#ifndef RESOURCE_IDENTIFIERS__H
#define RESOURCE_IDENTIFIERS__H
#include "ResourceHolder.hpp"

//temporary
#define TILEWIDTH 100.f
constexpr unsigned int WIDTH = 600 + TILEWIDTH;
constexpr unsigned int HEIGHT = 400 + TILEWIDTH;
/////////
namespace Fonts
{
	enum class ID
	{
		ButtonFont,
	};
}

namespace Textures
{
	enum class ID
	{
		ButtonTexture, PuzzleImage
	};
}

namespace SoundBuffers
{
	enum class ID
	{
		ButtonSound, TileSound
	};
}
typedef ResourceHolder<sf::Font, Fonts::ID> FontsHolder;
typedef ResourceHolder<sf::Texture, Textures::ID> TexturesHolder;
typedef ResourceHolder<sf::SoundBuffer, SoundBuffers::ID> SoundBuffersHolder;

#endif //RESOURCE_IDENTIFIERS__H
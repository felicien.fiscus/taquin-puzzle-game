#ifndef PUZZLE_H
#define PUZZLE_H

#include "ResourceIdentifiers.h"
/**

class Puzzle : responsible for the logic and user interface of a puzzle.
the puzzle is created by having the image sit in the top right corner of the window while there are grey tiles at the bottom and
on the right side while on the bottom right corner, there's an empty tile
*/

class Puzzle : public sf::Drawable
{
public:
	/*constructs a taquin puzzle by dividing the image stored in textures into a set of movable tiles
	*	contextWindow is used to get the cursor coordinates locally to the window
	*/

	Puzzle(const TexturesHolder& textures, const sf::RenderWindow& contextWindow);
	~Puzzle();

	//updates the puzzle, the logic happens in there
	void update(sf::Time dt);
	//events like going back to menu, exiting the game, but not the cursor movement, that is done in 'update'
	void handleEvent(const sf::Event& e);

private:
	//inherited from sf::Drawable
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	const sf::RenderWindow& fContextWindow;
	//sf::Vector2i getMouseVelDir();

	//Move the tile at the tileX, tileY grid coordinates towards dir
	void moveTile(int tileX, int tileY, sf::Vector2i dir);
	//Storage for the tiles
	std::vector<class Tile*> fTiles;

	//tile grid sizes
	unsigned int X_size = WIDTH / TILEWIDTH;
	unsigned int Y_size = HEIGHT / TILEWIDTH;

	//the shuffle will be animated
	bool fShuffle = false;

	sf::Vector2i fMousePosBuffer;	//used to compute the mouse velocity when you click and drag
	bool fCanMoveTiles = true;	//when other tiles are moving, the user cannot move any other tiles until it's finished moving
};

#endif //PUZZLE_H
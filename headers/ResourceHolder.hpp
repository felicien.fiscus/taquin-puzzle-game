#ifndef RESOURCE_HOLDER_HPP
#define RESOURCE_HOLDER_HPP

#include "SFML/Graphics.hpp"
#include "SFML/Audio/SoundBuffer.hpp"
#include <map>

/**
Resource manager class where the resource is keyed with a user defined ID 
a Resource can be anything that has a 'loadFromFile(const std::string&, secondParam)' method.
In our case, it can be sf::Texture, sf::SoundBuffer, sf::Font, etc.
*/

template<typename Resource, typename ID>
class ResourceHolder
{
	std::map<ID, Resource*> fResources;
public:
	ResourceHolder();

	void load(const std::string& FileName, ID id);

	template<typename Param>
	void load(const std::string& FileName, ID id, Param SecondParam);

	Resource& get(ID id);
	const Resource& get(ID id) const;
	
	~ResourceHolder();
};


#include "../src/ResourceHolder.inl"
#endif //RESOURCE_HOLDER_HPP
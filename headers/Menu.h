#ifndef MENU_H
#define MENU_H
#include "ResourceIdentifiers.h"


namespace TaquinGUI
{
	class Component;
	/**
	a Menu is simply a collection of Components put together
	*/
	class Menu : public sf::Drawable, public sf::Transformable
	{
	public:
		Menu(const sf::RenderWindow& contextWindow);
		~Menu();

		void update(sf::Time dt);
		void handleEvent(const sf::Event& e);

		//comp can be allocated in the heap as the Menu destructor takes responsibility for destroying the components
		//The function not only adds the component to fComponents, but also moves it a little bit so that the components don't overlap each other
		//It moves them from the top to the bottom, from the left to the right
		void addComp(Component* comp);

		sf::FloatRect getBoundingRect() const;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		std::vector<Component*> fComponents;
		const sf::RenderWindow& fContextWindow;
	};
}

#endif //MENU_H
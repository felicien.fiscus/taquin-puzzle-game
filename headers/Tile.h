#ifndef TILE_H
#define TILE_H
#include "ResourceIdentifiers.h"

/**
Tile class:
A tile is a movable part of the puzzle
the movement is fluid and so has a delay
*/
class Tile : public sf::Drawable, public sf::Transformable
{
public:
	/**
	@Param texRect : the little texture square representing the sprite of the tile
	*/
	Tile(const TexturesHolder& textures, sf::IntRect texRect);
	~Tile();

	//where the fluid movement happens
	void update(sf::Time dt);
	
	/**
	*@brief moves the tile by one tile unit in the direction pointed to by dir
	*@param \dir : the direction to move towards
	*/
	void moveTile(sf::Vector2i dir);
	
	bool isMoving() const;

	//The Puzzle class could potentially want to modify certain things
	sf::Sprite fSprite;
private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	bool fMoving;
	float fSpeed;
	sf::Vector2f fVelocity;

	sf::Time fMovingAccu;
	sf::Time fMovingDelay;
	//this is to ensure that the new position is the last one + TILEWIDTH
	sf::Vector2f fLastPosition;

};

#endif //TILE_H
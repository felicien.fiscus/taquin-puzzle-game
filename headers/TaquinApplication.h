#ifndef TAQUIN_APPLICATION_H
#define TAQUIN_APPLICATION_H
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "ResourceIdentifiers.h"
#include "Menu.h"


/*
The main application class where all the logic is.


*/
class Taquin
{
public:

	//@param imageFile : the image that will be divided into a taquin puzzle
	Taquin(const std::string& imageFile);
	~Taquin();

	/**method that contains the game loop**/
	void run();


private:

	/******resource loading*****
		@brief loads the different resources in #fTextures
				#fSounds and fFonts respectively
		@see ResourceIdentifiers.h
	
	*****/
	void loadTextures();
	void loadSounds();
	void loadFonts();
	/***************************/

	/******application states: the title menu state #Menu and the in game state #Game .
	*		The changing of states is handle via a switch statement in #run() method
	*****/
	enum class State
	{
		Menu, Game
	};
	State fCurrentState;	//to keep track of the current state the application is in
	/*******************/

	/*****resource holders*******/
	TexturesHolder fTextures;
	SoundBuffersHolder fSounds;
	FontsHolder fFonts;
	/**************************/

	//main rendering window 
	sf::RenderWindow fWindow;

	/**main loop methods called each frame*/
	void update(sf::Time dt);	//dt is the time between each frame
	void handleEvents();
	void render();

	//to keep track of whether the user is using the application or not
	bool fIsFocus;

	/*******States related attributes******/
	//menu state
	TaquinGUI::Menu fTitleMenu;
	void loadTitleMenu();
	
	//Game state
	//TaquinGUI::Menu fInGameMenu;
	class Puzzle* fPuzzle;

	//FileInput state
	TaquinGUI::Menu fFileInputMenu;
	void loadFileInputMenu();


	/***********************************/

};

#endif //TAQUIN_APPLICATION_H
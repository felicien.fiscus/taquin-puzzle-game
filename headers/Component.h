#ifndef COMPONENT_H
#define COMPONENT_H
#include "ResourceIdentifiers.h"
#include "SFML/Audio.hpp"
#include <functional>

namespace TaquinGUI
{
	/**
	Abstract class for a gui item, can be a button, a label, a text box, etc.
	a component can be clicked on, and if so fCallback will be called.
	
	*/
	class Component : public sf::Drawable, public sf::Transformable
	{
	public:
		Component(std::function<void()> callback);
		~Component();


		virtual void update(sf::Time dt) = 0;
		virtual void handleEvent(const sf::Event& e) = 0;

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
		
		//method to call when clicked on
		//it just calls fCallback by default
		virtual void click();
		//method to call when the cursor is hovering or not the component
		virtual void hover(bool flag);
		bool isHovering() const;

		virtual sf::FloatRect getBoundingRect() const = 0;
	protected:
		bool fIsHovering;
	private:

		std::function<void()> fCallback;

	};
}

#endif //COMPONENT_H
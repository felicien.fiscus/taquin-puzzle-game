#include "../headers/Button.h"
#include "../headers/UtilityFunc.h"

namespace TaquinGUI
{
	Button::Button(const TexturesHolder& textures, const FontsHolder& fonts, const SoundBuffersHolder& sounds, std::function<void()> callback, const std::string& text)
		:Component(callback), fNormalTexRect(0, 0, 80, 60), fHoverTexRect(80, 0, 80, 60),
		fButtonSprite(textures.get(Textures::ID::ButtonTexture), fNormalTexRect),
		fTextInButton(text, fonts.get(Fonts::ID::ButtonFont)), fClickSound(sounds.get(SoundBuffers::ID::ButtonSound))
	{
		if (text != "")
		{
			Utility::CenterOrigin(fTextInButton);
			sf::Vector2f button_wh(fButtonSprite.getLocalBounds().width, fButtonSprite.getLocalBounds().height);
			fTextInButton.setPosition(button_wh.x * 0.5f, button_wh.y * 0.5f);
		}
	}

	Button::~Button()
	{
	}

	void Button::update(sf::Time dt)
	{
	}

	void Button::click()
	{
		fClickSound.play();
		Component::click();
	}

	void Button::hover(bool flag)
	{
		Component::hover(flag);
		if (fIsHovering)
		{
			fButtonSprite.setTextureRect(fHoverTexRect);
		}
		else
		{
			fButtonSprite.setTextureRect(fNormalTexRect);
		}
	}

	void Button::setTexRect(sf::IntRect normalTexRect, sf::IntRect hoverTexRect, sf::IntRect clickedTexRect)
	{
		fNormalTexRect = normalTexRect;
		fHoverTexRect = hoverTexRect;
		//fClickedTexRect = clickedTexRect;
		//don't forget to update the sprite
		hover(isHovering());
	}

	sf::FloatRect Button::getBoundingRect() const
	{
		return fButtonSprite.getGlobalBounds();
	}

	void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		target.draw(fButtonSprite, states);
		if (fTextInButton.getString() != "")
		{
			target.draw(fTextInButton, states);
		}
	}
}
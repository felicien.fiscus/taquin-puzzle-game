#include "../headers/Component.h"

TaquinGUI::Component::Component(std::function<void()> callback)
	:fCallback(callback), fIsHovering(false)
{

}

TaquinGUI::Component::~Component()
{
}

void TaquinGUI::Component::click()
{
	fCallback();
}

void TaquinGUI::Component::hover(bool flag)
{
	fIsHovering = flag;
}

bool TaquinGUI::Component::isHovering() const
{
	return fIsHovering;
}

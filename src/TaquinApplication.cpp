#include "../headers/TaquinApplication.h"
#include "../headers/Button.h"
#include "../headers/UtilityFunc.h"
#include "../headers/Puzzle.h"

Taquin::Taquin(const std::string& imageFile)
	:fTextures(), fSounds(), fFonts(), fWindow(sf::VideoMode(WIDTH, HEIGHT), "Taquin", sf::Style::Close),
	fIsFocus(true), fTitleMenu(fWindow), fCurrentState(State::Menu), fFileInputMenu(fWindow)
{
	loadTextures();
	fTextures.load(imageFile, Textures::ID::PuzzleImage);
	loadSounds();
	loadFonts();

	fPuzzle = new Puzzle(fTextures, fWindow);

	loadTitleMenu();
}

Taquin::~Taquin()
{
	delete fPuzzle;
}

/**

deltaTime is used as a constant so that the number of update() calls 
per seconds is consistent.
Every time a iteration is made the 'loop_clock' is restarting and we add the elapsed time 
to 'Accu', when 'Accu' is greater than or equal to DeltaTime then and only then can we call update() and hangleEvents()
*/
void Taquin::run()
{
	sf::Clock loop_clock;
	unsigned int fps = 60;
	sf::Time deltaTime = sf::seconds(1.f / fps);
	sf::Time Accu = sf::Time::Zero;
	//start displaying the render window
	while (fWindow.isOpen())
	{
		Accu += loop_clock.restart();
		while (Accu >= deltaTime)
		{
			Accu -= deltaTime;

			if (fIsFocus)
			{
				update(deltaTime);
			}

			handleEvents();
		}

		render();
	}
	
}

void Taquin::loadTextures()
{
	fTextures.load("Textures/Buttons.png", Textures::ID::ButtonTexture);
}

void Taquin::loadSounds()
{
	fSounds.load("SoundEffects/buttonPressed.ogg", SoundBuffers::ID::ButtonSound);
}

void Taquin::loadFonts()
{
	fFonts.load("Fonts/future_thin.ttf", Fonts::ID::ButtonFont);
}

void Taquin::update(sf::Time dt)
{
	switch (fCurrentState)
	{
	case Taquin::State::Menu:
		fTitleMenu.update(dt);
		break;
	case Taquin::State::Game:
		fPuzzle->update(dt);
		break;
	default:
		break;
	}
}

void Taquin::handleEvents()
{
	sf::Event e;
	while (fWindow.pollEvent(e))
	{
		switch (e.type)
		{
		case sf::Event::Closed:
			fWindow.close();
			break;
		/*Decomment to only update the application when the user is focused on it*/
		//case sf::Event::GainedFocus:
		//	fIsFocus = true;
		//	break;
		//case sf::Event::LostFocus:
		//	fIsFocus = false;
		//	break;
		case sf::Event::KeyReleased:
			switch (e.key.code)
			{
			case sf::Keyboard::Escape:
				fCurrentState = State::Menu;
				break;
			default:
				break;
			}
			break;
		default:
			switch (fCurrentState)
			{
			case Taquin::State::Menu:
				fTitleMenu.handleEvent(e);
				break;
			case Taquin::State::Game:
				fPuzzle->handleEvent(e);
				break;
			default:
				break;
			}
			break;
		}
	}
}

void Taquin::render()
{
	fWindow.clear();
	switch (fCurrentState)
	{
	case Taquin::State::Menu:
		fWindow.draw(fTitleMenu);
		break;
	case Taquin::State::Game:
		fWindow.draw(*fPuzzle);
		break;
	default:
		break;
	}
	fWindow.display();
}

void Taquin::loadTitleMenu()
{
	using namespace TaquinGUI;
	Button* playButton = new Button(fTextures, fFonts, fSounds, [this]() {
		fCurrentState = State::Game;
		}, "");
	playButton->setTexRect(sf::IntRect(0, 0, 80, 60), sf::IntRect(80, 0, 80, 60));

	Button* quitButton = new Button(fTextures, fFonts, fSounds, [this]() { fWindow.close(); }, "");
	quitButton->setTexRect(sf::IntRect(0, 60, 80, 60), sf::IntRect(80, 60, 80, 60));

	fTitleMenu.addComp(playButton);
	fTitleMenu.addComp(quitButton);

	fTitleMenu.setPosition(WIDTH * 0.5f - fTitleMenu.getBoundingRect().width * 0.5f,
		HEIGHT * 0.5f - fTitleMenu.getBoundingRect().height * 0.5f);	//all of this just to center the menu
}

void Taquin::loadFileInputMenu()
{
}

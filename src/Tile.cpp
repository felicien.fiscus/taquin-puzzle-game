#include "../headers/Tile.h"
#include "../headers/UtilityFunc.h"

Tile::Tile(const TexturesHolder& textures, sf::IntRect texRect)
	:fSprite(textures.get(Textures::ID::PuzzleImage), texRect), fMoving(false), fSpeed(100.f), fMovingAccu(sf::Time::Zero),
	fMovingDelay(sf::seconds(texRect.width / fSpeed)), fVelocity(0.f, 0.f)
{
}

Tile::~Tile()
{
}

void Tile::update(sf::Time dt)
{
	if (fMoving)
	{
		move(fVelocity*dt.asSeconds());
		fMovingAccu += dt;
		if (fMovingAccu >= fMovingDelay)
		{
			fMovingAccu = sf::Time::Zero;
			fMoving = false;
			//making sure the floating point approximation didn't screw up by moving a bit too far or too short
			setPosition(fLastPosition + TILEWIDTH * fVelocity / Utility::length(fVelocity));
			fVelocity.x = fVelocity.y = 0.f;
		}
	}
}

void Tile::moveTile(sf::Vector2i dir)
{
	fVelocity = sf::Vector2f(dir) * fSpeed;
	fMoving = true;
	fLastPosition = getPosition();
}

bool Tile::isMoving() const
{
	return fMoving;
}

void Tile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(fSprite, states);
}

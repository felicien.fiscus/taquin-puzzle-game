#include "../headers/Puzzle.h"
#include "../headers/Tile.h"
#include "../headers/UtilityFunc.h"

Puzzle::Puzzle(const TexturesHolder& textures, const sf::RenderWindow& contextWindow)
	:fContextWindow(contextWindow), fTiles()
{
	fShuffle = true;
	/*initialising the tile grid*/
	for (int j = 0; j < Y_size; ++j)
	{
		for (int i = 0; i < X_size; ++i)
		{
			Tile* tile = new Tile(textures, sf::IntRect(i * TILEWIDTH, j * TILEWIDTH, TILEWIDTH, TILEWIDTH));
			tile->setPosition(i * TILEWIDTH, j * TILEWIDTH);
			fTiles.push_back(tile);
		}
	}
	//deleting the last one (bottom right corner)
	delete fTiles[fTiles.size() - 1];
	fTiles[fTiles.size() - 1] = nullptr;	//this will be the initial empty tile
	
	//bottom tiles
	for (int i = 0; i < X_size - 1; ++i)
	{
		fTiles[i + X_size * (Y_size - 1)]->fSprite.setColor(sf::Color::Magenta);
	}
	//right side tiles
	for (int j = 0; j < Y_size - 1; ++j)
	{
		fTiles[X_size - 1 + X_size * j]->fSprite.setColor(sf::Color::Magenta);
	}

}

Puzzle::~Puzzle()
{
	for (Tile* t : fTiles)
	{
		if (t)
			delete t;
	}
}

void Puzzle::update(sf::Time dt)
{
	fCanMoveTiles = true;
	for (Tile* t : fTiles)
	{
		if (t) {
			t->update(dt);
			fCanMoveTiles = fCanMoveTiles && !(t->isMoving());
		}
	}
	if (fShuffle)
	{
		//shuffle the tiles
		if (fCanMoveTiles)
		{
			//coordinates of the empty tile
			float X = 0.f, Y = 0.f;
			//find the empty tile
			for (int x = 0; x < X_size; ++x)
			{
				for (int y = 0; y < Y_size; ++y)
				{
					if (!fTiles[x + y * X_size])
					{
						X = x;
						Y = y;
						break;	//there's only one empty tile
					}
				}
			}
			//move one random tile around it
			std::pair<sf::Vector2i, sf::Vector2i> tab[4] = { std::make_pair(sf::Vector2i(X, Y - 1), sf::Vector2i(0,1)),
															std::make_pair(sf::Vector2i(X, Y + 1), sf::Vector2i(0,-1)),
															std::make_pair(sf::Vector2i(X - 1, Y), sf::Vector2i(1,0)),
															std::make_pair(sf::Vector2i(X + 1, Y), sf::Vector2i(-1,0))
															};
			auto tab_pair = tab[Utility::randomInt(4)];
			moveTile(tab_pair.first.x, tab_pair.first.y, tab_pair.second);
		}
	}
}

void Puzzle::handleEvent(const sf::Event& e)
{
	switch (e.type)
	{
	case sf::Event::MouseButtonPressed:
		fMousePosBuffer = sf::Mouse::getPosition(fContextWindow);
		break;
	case sf::Event::MouseButtonReleased:
		//potentially move one tile
		if (fCanMoveTiles)
		{
			sf::Vector2f mouseVel = sf::Vector2f(sf::Mouse::getPosition(fContextWindow)) - sf::Vector2f(fMousePosBuffer);
			float ps_x = mouseVel.x, ps_y = mouseVel.y;	//we compare the projection in each direction to then see which is larger
			//if the x is larger, than the direction is X, otherwise it's Y
			sf::Vector2i mouseDir((abs(ps_x) >= abs(ps_y)) ? sf::Vector2i(ps_x / abs(ps_x), 0) : sf::Vector2i(0, ps_y / abs(ps_y)));

			moveTile(fMousePosBuffer.x / TILEWIDTH, fMousePosBuffer.y / TILEWIDTH, mouseDir);
		}
		break;
	case sf::Event::KeyPressed:
		switch (e.key.code)
		{
		case sf::Keyboard::S:
			fShuffle = !fShuffle;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
}

void Puzzle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (Tile* t : fTiles)
	{
		if(t)
			target.draw(*t, states);
	}
}

void Puzzle::moveTile(int tileX, int tileY, sf::Vector2i dir)
{
	//check if the cursor is out of bounds
	if (tileX >= X_size || tileX < 0 || tileY >= Y_size || tileY < 0)
		return;

	//check whether it's an empty tile and if it's able to move
	if (fTiles[tileX + tileY * X_size] && !fTiles[tileX + tileY * X_size]->isMoving())
	{
		//check whether the tile isn't moving towards an out of bounds zone
		switch (dir.x)
		{
		case 1:	//goes to the right
			if (tileX + 1 >= X_size)
				return;
			break;
		case -1:	//goes to the left
			if (tileX - 1 < 0)
				return;
			break;
		case 0:
			switch (dir.y)
			{
			case 1:	//goes downwards
				if (tileY + 1 >= Y_size)
					return;
				break;
			case -1:	//goes upwards
				if (tileY - 1 < 0)
					return;
				break;
			default:
				break;
			}
		default:
			break;
		}

		//and finally, check if it is correctly moving towards an empty tile or not
		if (fTiles[tileX + dir.x + (tileY + dir.y) * X_size])	//i.e. the tile is not empty
			return;

		//now we can move the tile
		fTiles[tileX + tileY * X_size]->moveTile(dir);	//physically moving it
		//moving it in memory
		fTiles[tileX + dir.x + (tileY + dir.y) * X_size] = fTiles[tileX + tileY * X_size];
		fTiles[tileX + tileY * X_size] = nullptr;
	}
}

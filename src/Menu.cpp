#include "../headers/Menu.h"
#include "../headers/Component.h"

TaquinGUI::Menu::Menu(const sf::RenderWindow& contextWindow)
	:fComponents(), fContextWindow(contextWindow)
{
}

TaquinGUI::Menu::~Menu()
{
	for (Component* comp : fComponents)
	{
		if (comp)
			delete comp;
	}
}

void TaquinGUI::Menu::update(sf::Time dt)
{
	for (Component* comp : fComponents)
	{
		//we need to take into account the position of the menu itself
		sf::FloatRect rect(getPosition() + comp->getPosition(), sf::Vector2f(comp->getBoundingRect().width, comp->getBoundingRect().height));
		//test whether the mouse is hovering the component and set the flag accordingly
		comp->hover(rect.contains(sf::Vector2f(sf::Mouse::getPosition(fContextWindow))));
		comp->update(dt);
	}
}

void TaquinGUI::Menu::handleEvent(const sf::Event& e)
{
	for (Component* c : fComponents)
	{
		switch (e.type)
		{
		case sf::Event::MouseButtonPressed:
			if (c->isHovering())
				c->click();
			break;
		default:
			break;
		}
		c->handleEvent(e);
	}
	
}

void TaquinGUI::Menu::addComp(Component* comp)
{
	assert(comp);
	size_t sz = fComponents.size();
	//if it's not the first component of the menu, move it so it's not overlapping
	if (sz > 0)
	{
		Component* last = fComponents[sz - 1];
		sf::FloatRect last_rect(last->getPosition().x, last->getPosition().y, last->getBoundingRect().width, last->getBoundingRect().height);
		//when the bottom is reached, we need to move further to the right and go back up
		int top_limit = HEIGHT / last_rect.height;	//how many last_rect we can fit on the screen
		//how much to the right we need to go
		float left_offset = int(sz / top_limit) * last_rect.width + bool(sz / top_limit) * 10.f;	//the +10.f is so that the components aren't sticked together
		//how much downwards we need to go
		float top_offset = last_rect.height * (sz % top_limit) + bool(sz % top_limit) * 10.f;	//the +10.f is so that the components aren't sticked together
		comp->setPosition(last_rect.left + left_offset, last_rect.top + top_offset);
	}

	fComponents.push_back(comp);
}

sf::FloatRect TaquinGUI::Menu::getBoundingRect() const
{
	sf::FloatRect rect;
	if (fComponents.size() == 0) return rect;
	rect.left = fComponents[0]->getPosition().x + getPosition().x;
	rect.top = fComponents[0]->getPosition().y + getPosition().y;
	rect.width = abs(fComponents[fComponents.size() - 1]->getPosition().x + fComponents[fComponents.size() - 1]->getBoundingRect().width
		- fComponents[0]->getPosition().x);
	//TODO: do better, this doesn't work for all cases
	rect.height = abs(fComponents[fComponents.size() - 1]->getPosition().y + fComponents[fComponents.size() - 1]->getBoundingRect().height
		- fComponents[0]->getPosition().y);

	return rect;
}

void TaquinGUI::Menu::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	for (Component* c : fComponents)
		target.draw(*c, states);
}

#include "../headers/TaquinApplication.h"

int main(int argc, char** argv)
{
	std::string fileImage = "Images/default.png";
	if (argc > 1)
	{
		fileImage = argv[1];
	}
	try
	{
		Taquin taquin(fileImage);
		taquin.run();
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		system("pause");
		return 1;
	}

	return 0;
}